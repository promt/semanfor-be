package cz.petrjanata.semanfor.model

/**
 * Created by Petr Janata on 13.08.2018.
 */
class User() {

    constructor(serverAuthCode: String): this() {
        this.serverAuthCode = serverAuthCode
    }

    constructor(serverAuthCode: String, pushToken: String): this(serverAuthCode) {
        this.pushToken = pushToken
    }

    lateinit var serverAuthCode: String
    lateinit var pushToken: String
    lateinit var email: String
    lateinit var token: String

    val calendarID: String
        get() = email
}
