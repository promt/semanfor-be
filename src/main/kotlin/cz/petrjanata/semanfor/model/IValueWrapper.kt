package cz.petrjanata.semanfor.model

interface IValueWrapper<V, E : SemanforException> {
    val value: V?
    val exception: E?

    @Throws(IllegalStateException::class)
    fun validate() {
        if (value == null && exception == null) {
            throw IllegalStateException("Both value and exception cannot be null in the same time.")
        }
    }
}