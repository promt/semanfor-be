package cz.petrjanata.semanfor.helper.inject

import com.github.salomonbrys.kodein.*
import cz.petrjanata.semanfor.ProgramConstants
import cz.petrjanata.semanfor.api.ApiConstants
import cz.petrjanata.semanfor.helper.ExecutionHelper
import cz.petrjanata.semanfor.service.configuration.*
import cz.petrjanata.semanfor.service.web.WebConstants
import cz.petrjanata.semanfor.service.web.WebServiceHelper
import cz.petrjanata.semanfor.service.web.firebase.FirebaseWebService
import cz.petrjanata.semanfor.service.web.google.GoogleWebConstants
import cz.petrjanata.semanfor.service.web.google.GoogleWebService
import cz.petrjanata.semanfor.service.web.retrofit.OkHttpClientHelper
import cz.petrjanata.semanfor.usecase.DriverUsecase
import cz.petrjanata.semanfor.usecase.firebase.FirebaseUsecase
import cz.petrjanata.semanfor.usecase.google.EventUsecase
import cz.petrjanata.semanfor.usecase.google.ProvideWebUserTokenUsecase
import cz.petrjanata.semanfor.usecase.google.VerifyTokenUsecase
import cz.petrjanata.semanfor.usecase.place.GetPlaceUsecase
import cz.petrjanata.semanfor.usecase.place.RequestPlacesUsecase
import cz.petrjanata.semanfor.usecase.place.ReservePlaceUsecase

class SemanforKodeinHelper {

    fun provideKodein(): Kodein {
        return Kodein {

            ///servicies
            bind<WebServiceHelper>() with singleton { WebServiceHelper() }
            bind<IParkPlacesProvider>() with provider { KotlinParkPlacesProvider }
            bind<IDriversProvider>() with provider { KotlinDriversProvider }
            bind<GoogleWebService>() with singleton { GoogleWebService(kodein.instance()) }
            bind<FirebaseWebService>() with singleton { FirebaseWebService(kodein.instance()) }
            bind<OkHttpClientHelper>() with provider { OkHttpClientHelper() }


            ///use cases
            bind<VerifyTokenUsecase>() with provider { VerifyTokenUsecase() }
            bind<ProvideWebUserTokenUsecase>() with provider { ProvideWebUserTokenUsecase() }
            bind<EventUsecase>() with provider { EventUsecase() }
            bind<ReservePlaceUsecase>() with provider { ReservePlaceUsecase() }
            bind<GetPlaceUsecase>() with provider { GetPlaceUsecase() }
            bind<RequestPlacesUsecase>() with provider { RequestPlacesUsecase() }
            bind<FirebaseUsecase>() with provider { FirebaseUsecase() }
            bind<DriverUsecase>() with provider { DriverUsecase() }

            ///constants
            bind<WebConstants>() with provider { WebConstants }
            bind<GoogleWebConstants>() with provider { GoogleWebConstants }
            bind<ConfigurationValues>() with provider { ConfigurationValues }
            bind<ProgramConstants>() with provider { ProgramConstants }
            bind<ApiConstants>() with provider { ApiConstants }

            ///utils
            bind<ExecutionHelper>() with provider { ExecutionHelper() }
        }
    }
}