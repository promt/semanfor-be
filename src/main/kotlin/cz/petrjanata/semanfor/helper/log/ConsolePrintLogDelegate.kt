package cz.petrjanata.semanfor.helper.log

/**
 * Created by Petr Janata on 15.03.2018.
 */

internal class ConsolePrintLogDelegate : ASemanforLogDelegate() {

    override fun isLoggable(s: String?, i: Int): Boolean {
        return true
    }

    override fun println(priority: Int, tag: String?, msg: String?): Int {
        if (priority >= ILogDelegate.WARN) {
            System.err.println(String.format("%s: %s", tag, msg))
        } else {
            println(String.format("%s: %s", tag, msg))
        }

        return 0
    }
}
