package cz.petrjanata.semanfor.helper.log


/**
 * Created by Petr Janata on 15.03.2018.
 */

internal interface ILogDelegate {

    companion object {

        /**
         * Priority constant for the println method; use SLog.v.
         */
        const val VERBOSE = 2

        /**
         * Priority constant for the println method; use SLog.d.
         */
        const val DEBUG = 3

        /**
         * Priority constant for the println method; use SLog.i.
         */
        const val INFO = 4

        /**
         * Priority constant for the println method; use SLog.w.
         */
        const val WARN = 5

        /**
         * Priority constant for the println method; use SLog.e.
         */
        const val ERROR = 6

        /**
         * Priority constant for the println method.
         */
        const val ASSERT = 7
    }

    fun v(tag: String?, msg: String?): Int

    fun v(tag: String?, msg: String?, tr: Throwable?): Int

    fun d(tag: String?, msg: String?): Int

    fun d(tag: String?, msg: String?, tr: Throwable?): Int

    fun i(tag: String?, msg: String?): Int

    fun i(tag: String?, msg: String?, tr: Throwable?): Int

    fun w(tag: String?, msg: String?): Int

    fun w(tag: String?, msg: String?, tr: Throwable?): Int

    fun isLoggable(s: String?, i: Int): Boolean

    fun w(tag: String?, tr: Throwable?): Int

    fun e(tag: String?, msg: String?): Int

    fun e(tag: String?, msg: String?, tr: Throwable?): Int

    fun wtf(tag: String?, msg: String?): Int

    fun wtf(tag: String?, tr: Throwable?): Int

    fun wtf(tag: String?, msg: String?, tr: Throwable?): Int

    fun getStackTraceString(tr: Throwable?): String

    fun println(priority: Int, tag: String?, msg: String?): Int
}
