package cz.petrjanata.semanfor.helper


import java.util.concurrent.Executor
import java.util.concurrent.Executors


class ExecutionHelper {

    private val bgExecutor: Executor by lazy { Executors.newCachedThreadPool() }

    fun executeAsync(task: ()->Unit) {
        bgExecutor.execute(task)
    }
}
