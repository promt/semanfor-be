package cz.petrjanata.semanfor.service.web.google

import cz.petrjanata.semanfor.model.ConnectionException
import cz.petrjanata.semanfor.model.IValueWrapper
import cz.petrjanata.semanfor.service.web.WebConstants
import cz.petrjanata.semanfor.service.web.WebResponse
import cz.petrjanata.semanfor.service.web.WebServiceHelper
import cz.petrjanata.semanfor.service.web.google.model.EventDTO
import cz.petrjanata.semanfor.service.web.google.model.EventsListDTO
import cz.petrjanata.semanfor.service.web.retrofit.OkHttpClientHelper
import cz.petrjanata.semanfor.service.web.retrofit.RetrofitConfiguration
import retrofit2.Call
import java.text.SimpleDateFormat
import java.util.*

class GoogleWebService(private val webServiceHelper: WebServiceHelper) {


    private val TOKEN_PREFIX: String = "Bearer "
    private val dateFormat = SimpleDateFormat(GoogleWebConstants.GOOGLE_JSON_DATE_FORMAT)
    private val baseUrl: String = GoogleWebConstants.GOOGLE_URL_CALENDAR
    private val headers: Map<String, String> = mapOf(WebConstants.CONTENT_TYPE to WebConstants.APPLICATION_JSON)
    private val okClientProvider: OkHttpClientHelper = OkHttpClientHelper()
    private val retrofitProtocol: GoogleRetrofitProtocol = RetrofitConfiguration().getApiCaller(baseUrl, headers, GoogleRetrofitProtocol::class.java, okClientProvider)


    fun getEvent(token: String, calendarID: String, eventID: String): IValueWrapper<EventDTO, ConnectionException> {
        val authorization = TOKEN_PREFIX + token
        val apiCall: Call<EventDTO> = retrofitProtocol.getEvent(authorization, calendarID, eventID)
        return webServiceHelper.executeApiCall(apiCall)
    }

    fun getEvents(token: String, calendarId: String, intervalStart: Date, intervalEnd: Date, nextPageToken: String?): IValueWrapper<EventsListDTO, ConnectionException> {
        val timeMin = dateFormat.format(intervalStart)
        val timeMax = dateFormat.format(intervalEnd)
        val authorization = TOKEN_PREFIX + token
        val apiCall: Call<EventsListDTO> = retrofitProtocol.getEvents(authorization, calendarId, timeMin, timeMax, nextPageToken, true)
        return webServiceHelper.executeApiCall(apiCall)
    }

    fun createEvent(token: String, calendarID: String, event: EventDTO): IValueWrapper<EventDTO, ConnectionException> {
        val authorization = TOKEN_PREFIX + token
        val apiCall: Call<EventDTO> = retrofitProtocol.createEvent(authorization, calendarID, event)
        return webServiceHelper.executeApiCall(apiCall)
    }

    fun updateEvent(token: String, calendarID: String, eventID: String, eventDTO: EventDTO): IValueWrapper<EventDTO, ConnectionException> {
        val authorization = TOKEN_PREFIX + token
        val apiCall: Call<EventDTO> = retrofitProtocol.updateEvent(authorization, calendarID, eventID, eventDTO)
        return webServiceHelper.executeApiCall(apiCall)
    }

    fun deleteEvent(token: String, calendarID: String, id: String): IValueWrapper<WebResponse<EventDTO>, ConnectionException> {
        val authorization = TOKEN_PREFIX + token
        val apiCall: Call<EventDTO> = retrofitProtocol.deleteEvent(authorization, calendarID, id)
        return webServiceHelper.executeApiCallWithCode(apiCall)
    }
}