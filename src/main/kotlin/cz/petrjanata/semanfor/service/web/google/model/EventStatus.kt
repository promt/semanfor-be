package cz.petrjanata.semanfor.service.web.google.model

enum class EventStatus {
    confirmed, tentative, cancelled
}
