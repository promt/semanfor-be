package cz.petrjanata.semanfor.service.web.google.model

import com.google.gson.annotations.SerializedName

enum class EventVisibility {
    @SerializedName("public")
    PUBLIC,
    @SerializedName("private")
    PRIVATE
}