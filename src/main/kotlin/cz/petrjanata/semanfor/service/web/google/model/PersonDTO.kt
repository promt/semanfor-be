package cz.petrjanata.semanfor.service.web.google.model

import com.google.gson.annotations.SerializedName
import cz.petrjanata.semanfor.service.web.IWebDTO

class PersonDTO : IWebDTO {
        @SerializedName("id")
        lateinit var id: String
        @SerializedName("email")
        lateinit var email: String
        @SerializedName("name")
        var name: String? = null
        @SerializedName("responseStatus")
        var responseStatus: ResponseStatus? = null

    constructor()

    constructor(email: String, responseStatus: ResponseStatus?) {
        this.email = email
        this.responseStatus = responseStatus
    }

}