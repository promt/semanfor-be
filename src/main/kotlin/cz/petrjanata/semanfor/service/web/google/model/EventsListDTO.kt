package cz.petrjanata.semanfor.service.web.google.model

import com.google.gson.annotations.SerializedName
import cz.petrjanata.semanfor.service.web.IWebDTO

/**
 * Wrapper for list of @see [EventDTO]
 *
 * @see [Events list](https://developers.google.com/google-apps/calendar/v3/reference/events/list)
 * Created by Petr Janata on 31.07.2018.
 */

class EventsListDTO : IWebDTO {

        @SerializedName("pageToken")
                 var nextPageToken: String? = null
        @SerializedName("items")
        lateinit var events: List<EventDTO>

}
