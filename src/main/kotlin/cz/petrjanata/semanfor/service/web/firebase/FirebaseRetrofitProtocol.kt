package cz.petrjanata.semanfor.service.web.firebase

import cz.petrjanata.semanfor.service.web.firebase.model.PushBodyDTO
import cz.petrjanata.semanfor.service.web.firebase.model.FirebaseResponseDTO
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface FirebaseRetrofitProtocol {

    @POST("send")
    fun sendPush(@Body pushBodyDTO: PushBodyDTO): Call<FirebaseResponseDTO>

}