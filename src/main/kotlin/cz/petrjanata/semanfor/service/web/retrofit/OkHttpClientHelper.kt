package cz.petrjanata.semanfor.service.web.retrofit

import cz.petrjanata.semanfor.helper.log.SLog
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor

class OkHttpClientHelper {
    fun provide(headers: Map<String, String>?): OkHttpClient {
        return getClientWithCertificate(headers)
    }

    fun getClientWithCertificate(headers: Map<String, String>?): OkHttpClient {
        val logger = okhttp3.logging.HttpLoggingInterceptor.Logger {SLog.v("cz.petrjanata.semanfor.retrofit",it)}
        val loggingInterceptor = HttpLoggingInterceptor(logger)
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

        val clientBuilder = OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .addInterceptor { chain ->
                    val request = chain.request()
                    val requestBuilder: Request.Builder = request.newBuilder()

                    requestBuilder.addHeader("Accept", "application/json")
                    if (headers != null) {
                        for ((key, value) in headers) {
                            requestBuilder.addHeader(key, value)
                            SLog.v("Header: $key = $value")
                        }
                    }
                    chain.proceed(requestBuilder.build())
                }


//        clientBuilder.sslSocketFactory(sslProvider.socketFactory, sslProvider.trustManagers[0] as X509TrustManager)

        return clientBuilder.build()
    }

}