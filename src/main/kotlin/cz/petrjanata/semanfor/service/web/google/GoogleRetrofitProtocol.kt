package cz.petrjanata.semanfor.service.web.google

import cz.petrjanata.semanfor.service.web.WebConstants
import cz.petrjanata.semanfor.service.web.google.model.EventDTO
import cz.petrjanata.semanfor.service.web.google.model.EventsListDTO
import retrofit2.Call
import retrofit2.http.*

interface GoogleRetrofitProtocol {

    @GET("/calendar/v3/calendars/{calendarId}/events/{event_id}")
    fun getEvent(@Header(WebConstants.AUTHORIZATION) authorization: String,
                 @Path("calendarId") calendarId: String,
                 @Path("event_id") eventId: String): Call<EventDTO>

    @GET("calendar/v3/calendars/{calendarId}/events")
    fun getEvents(@Header(WebConstants.AUTHORIZATION) authorization: String,
                  @Path("calendarId") calendarId: String,
                  @Query("timeMin") timeMin: String,
                  @Query("timeMax") timeMax: String,
                  @Query("pageToken") nextPageToken: String?,
                  @Query("singleEvents") showAsSingle: Boolean): Call<EventsListDTO>

    @POST("calendar/v3/calendars/{calendarId}/events")
    fun createEvent(@Header(WebConstants.AUTHORIZATION) authorization: String,
                    @Path("calendarId") calendarId: String,
                    @Body event: EventDTO): Call<EventDTO>

    @PATCH("calendar/v3/calendars/{calendarId}/events/{eventId}")
    fun updateEvent(@Header(WebConstants.AUTHORIZATION) authorization: String,
                    @Path("calendarId") calendarId: String,
                    @Path("eventId") eventId: String,
                    @Body eventDTO: EventDTO): Call<EventDTO>

    @DELETE("calendar/v3/calendars/{calendarId}/events/{eventId}")
    fun deleteEvent(@Header(WebConstants.AUTHORIZATION) authorization: String,
                    @Path("calendarId") calendarId: String,
                    @Path("eventId") id: String): Call<EventDTO>
}