package cz.petrjanata.semanfor.service.web.retrofit

import com.github.salomonbrys.kodein.instance
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import cz.petrjanata.semanfor.Application
import cz.petrjanata.semanfor.api.ApiConstants
import cz.petrjanata.semanfor.service.web.WebConstants
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitConfiguration {

    private val webConstants: WebConstants = Application.kodein.instance()
    private var retrofit: Retrofit? = null


    fun <T> getApiCaller(baseUrl: String,
                     headers: Map<String, String>?,
                     retrofitServiceClass: Class<T>,
                     okHttpClientProvider: OkHttpClientHelper = OkHttpClientHelper()
    ): T {
        val client: OkHttpClient = okHttpClientProvider.provide(headers)
        val retrofitProtocol: T


        retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(getGson()))
                .build()

        retrofitProtocol = retrofit!!.create(retrofitServiceClass)
        return retrofitProtocol
    }

    private fun getGson(): Gson {
        return GsonBuilder()
                .setDateFormat(webConstants.JSON_DATE_FORMAT)
                .create()
    }
}