package cz.petrjanata.semanfor.service.web.google

/**
 * This class is holder for constants used in communication with other web services.
 * Keep values of these constants according to the services public interface.
 */
object GoogleWebConstants {

    ///Google
    ///Google user info keys
    const val GOOGLE_KEY_USER_NAME = "name"
    const val GOOGLE_KEY_USER_PICTURE = "picture"
    const val GOOGLE_KEY_USER_LOCALE = "locale"
    const val GOOGLE_KEY_USER_FAMILY_NAME = "family_name"
    const val GOOGLE_KEY_USER_GIVEN_NAME = "given_name"

    ///Google date format
    const val GOOGLE_JSON_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSZ"

    ///Google urls
    const val GOOGLE_URL_TOKEN_SERVER_ENCODED = "https://www.googleapis.com/oauth2/v4/token"
    const val GOOGLE_URL_CALENDAR = "https://www.googleapis.com/"

}