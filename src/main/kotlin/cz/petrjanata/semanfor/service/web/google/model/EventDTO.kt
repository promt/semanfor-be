package cz.petrjanata.semanfor.service.web.google.model

import com.google.gson.annotations.SerializedName
import cz.petrjanata.semanfor.service.web.IWebDTO


/**
 * Event data transfer object for google calendar
 * @see [Events](https://developers.google.com/google-apps/calendar/v3/reference/events)
 * Created by Petr Janata on 31.07.2018.
 */

class EventDTO() : IWebDTO {

    @SerializedName("summary")
    lateinit var name: String
    @SerializedName("id")
    lateinit var id: String
    @SerializedName("description")
             var description: String? = null
    @SerializedName("end")
    lateinit var end: EventTime
    @SerializedName("start")
    lateinit var start: EventTime
    @SerializedName("creator")
    lateinit var creator: PersonDTO
    @SerializedName("organizer")
             var organizer: PersonDTO? = null
    @SerializedName("attendees")
             var attendees: List<PersonDTO> = emptyList()
    @SerializedName("status")
             var status: EventStatus? = null
    @SerializedName("visibility")
             var visibility: EventVisibility? = null

    constructor(name: String, timeFrom: Long, timeTo: Long, attendees: List<PersonDTO>) : this() {
        this.name = name
        this.end = EventTime(timeTo)
        this.start = EventTime(timeFrom)
        this.attendees = attendees
    }

}
