package cz.petrjanata.semanfor.service.web

import cz.petrjanata.semanfor.service.web.google.GoogleWebConstants

object WebConstants {
    const val JSON_DATE_FORMAT = GoogleWebConstants.GOOGLE_JSON_DATE_FORMAT
    const val AUTHORIZATION = "Authorization"
    const val CONTENT_TYPE = "Content-Type"
    const val APPLICATION_JSON = "application/json"

}