package cz.petrjanata.semanfor.service.web.google.model

import com.google.gson.annotations.SerializedName
import java.util.*

class EventTime {
    @SerializedName("date")
    private val date: Date? = null

    @SerializedName("dateTime")
    private val dateTime: Date?


    constructor(time: Long) {
        dateTime = Date(time)
    }

    constructor(date: Date) {
        dateTime = date
    }

    fun getDate(): Date {
        return dateTime ?: date ?: throw IllegalArgumentException("Missing parameter date or dateTime in class EventTime.")
    }
}