package cz.petrjanata.semanfor.service.web

data class WebResponse<out T:IWebDTO>(val responseDto: T?, val code: Int)