package cz.petrjanata.semanfor.service.web.google.model

enum class ResponseStatus {
    needsAction, declined, tentative, accepted
}
