package cz.petrjanata.semanfor.service.web.firebase

import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.Application
import cz.petrjanata.semanfor.api.model.PushDataWrapperADO
import cz.petrjanata.semanfor.model.ConnectionException
import cz.petrjanata.semanfor.model.IValueWrapper
import cz.petrjanata.semanfor.service.web.WebConstants.APPLICATION_JSON
import cz.petrjanata.semanfor.service.web.WebConstants.AUTHORIZATION
import cz.petrjanata.semanfor.service.web.WebConstants.CONTENT_TYPE
import cz.petrjanata.semanfor.service.web.WebServiceHelper
import cz.petrjanata.semanfor.service.web.firebase.model.PushBodyDTO
import cz.petrjanata.semanfor.service.web.firebase.model.FirebaseResponseDTO
import cz.petrjanata.semanfor.service.web.retrofit.OkHttpClientHelper
import cz.petrjanata.semanfor.service.web.retrofit.RetrofitConfiguration
import retrofit2.Call

class FirebaseWebService(private val webServiceHelper: WebServiceHelper) {

    private val serverKey: String = FirebaseWebConstants.FIREBASE_SERVER_KEY
    private val baseUrl: String = FirebaseWebConstants.FIREBASE_URL
    private val headers: Map<String, String> = mapOf(
            CONTENT_TYPE to APPLICATION_JSON,
            AUTHORIZATION to "key=$serverKey"
    )
    private val okClientProvider: OkHttpClientHelper = Application.kodein.instance()
    private val retrofitProtocol: FirebaseRetrofitProtocol

    init {
        retrofitProtocol = RetrofitConfiguration().getApiCaller(baseUrl, headers, FirebaseRetrofitProtocol::class.java, okClientProvider)
    }

    fun sendPush(pushToken: String, pushDataWrapperADO: PushDataWrapperADO): IValueWrapper<FirebaseResponseDTO, ConnectionException> {
        val apiCall: Call<FirebaseResponseDTO> = retrofitProtocol.sendPush(PushBodyDTO(pushDataWrapperADO, pushToken))
        return webServiceHelper.executeApiCall(apiCall)
    }

}