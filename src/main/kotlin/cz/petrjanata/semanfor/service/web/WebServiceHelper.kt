package cz.petrjanata.semanfor.service.web

import cz.petrjanata.semanfor.helper.log.SLog
import cz.petrjanata.semanfor.model.ConnectionException
import cz.petrjanata.semanfor.model.IValueWrapper
import cz.petrjanata.semanfor.model.UnsupportedResponseException
import cz.petrjanata.semanfor.model.ValueWrapper
import retrofit2.Call
import retrofit2.Response
import java.io.IOException

class WebServiceHelper {


    ///methods for delegates
    internal fun <T : IWebDTO> executeApiCall(
            apiCall: Call<T>
    ): IValueWrapper<T, ConnectionException> {
        val responseValueWrapper = executeApiCallWithCode(apiCall)

        val code = responseValueWrapper.value?.code ?: return ValueWrapper(responseValueWrapper.exception)
        val responseDTO = responseValueWrapper.value?.responseDto
        if (responseDTO is List<*>) {
            val message = "Do not use this method for lists. Use overloaded variant with "
            val ex = UnsupportedOperationException(message)
            SLog.e(message, ex)
            return ValueWrapper(ConnectionException(ex))
        } else {
            return ValueWrapper(responseDTO)
        }
    }


    internal fun <T : IWebDTO> executeApiCallWithCode(apiCall: Call<T>): IValueWrapper<WebResponse<T>, ConnectionException> {

        val response: Response<T>
        try {
            response = apiCall.execute()
        } catch (ex: IOException) {
            return ValueWrapper(ConnectionException())
        }

        if (response.isSuccessful) {
            return onResponse(response)
        } else {
            return ValueWrapper(ConnectionException())
        }

    }

    ///private methods
    private fun <T : IWebDTO> onResponse(response: Response<T>?): IValueWrapper<WebResponse<T>, ConnectionException> {
        when (response?.code()) {
            200, 201 -> {
                val body: T = response.body()
                return ValueWrapper(WebResponse(body, response.code()))
            }

            202, 204, 304 -> return ValueWrapper(WebResponse(null, response.code()), null)
            else -> {
                val ex = UnsupportedResponseException("" + response?.errorBody()?.source())
                return ValueWrapper(ex)
            }
        }
    }

}