package cz.petrjanata.semanfor.service.web.firebase.model

import cz.petrjanata.semanfor.api.model.PushDataWrapperADO
import cz.petrjanata.semanfor.service.web.IWebDTO

class PushBodyDTO() : IWebDTO {

    lateinit var data: PushDataWrapperADO
    /** Push token.*/
    lateinit var to: String

    constructor(data: PushDataWrapperADO, to: String): this() {
        this.data = data
        this.to = to
    }
}