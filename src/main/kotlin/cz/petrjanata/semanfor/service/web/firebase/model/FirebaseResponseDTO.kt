package cz.petrjanata.semanfor.service.web.firebase.model

import com.google.gson.JsonElement
import cz.petrjanata.semanfor.service.web.IWebDTO

class FirebaseResponseDTO : IWebDTO {
             var success: Int = 0
             var failure: Int = 0
    lateinit var results: JsonElement
}