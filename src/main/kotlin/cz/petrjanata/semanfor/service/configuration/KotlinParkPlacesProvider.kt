package cz.petrjanata.semanfor.service.configuration

import cz.petrjanata.semanfor.api.model.PlaceADO


object KotlinParkPlacesProvider : IParkPlacesProvider {

    override val allPlaces: Collection<PlaceADO> by lazy { composeAllPlaces() }

    override val placesForReservation: List<PlaceADO> = listOf(
            PlaceADO(
                    "15",
                    "PHA-PARKING-č.15",
                    "eman.cz_3533373235333834323233@resource.calendar.google.com"
            ),
            PlaceADO(
                    "16",
                    "PHA-PARKING-č.16",
                    "eman.cz_35333639393538353535@resource.calendar.google.com"
            ),
            PlaceADO(
                    "17",
                    "PHA-PARKING-č.17",
                    "eman.cz_2d3636333437333230393935@resource.calendar.google.com"
            )
    )


    private fun composeAllPlaces(): Collection<PlaceADO> {
        return placesForReservation.union(emptyList())
    }



}