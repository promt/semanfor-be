package cz.petrjanata.semanfor.service.configuration

import cz.petrjanata.semanfor.api.model.DriverADO

interface IDriversProvider {
    val allDrivers: List<DriverADO>
}
