package cz.petrjanata.semanfor.service.configuration

import cz.petrjanata.semanfor.api.model.PlaceADO

interface IParkPlacesProvider {
    val allPlaces: Collection<PlaceADO>
    val placesForReservation: Collection<PlaceADO>
}