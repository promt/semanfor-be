package cz.petrjanata.semanfor.service.configuration

import cz.petrjanata.semanfor.api.model.DriverADO

object KotlinDriversProvider : IDriversProvider {
    override val allDrivers: List<DriverADO> = listOf(
            //todo fill your drivers
            DriverADO("Rezervace Parkoviště", "parking@eman.cz", "+420 777 666 555", "Trabant", "PAH0011"),
            DriverADO("Petr Janata", "petr.janata@eman.cz", "+420 765 222 444", "VW Golf modrý", "PJH0022")
    )
}