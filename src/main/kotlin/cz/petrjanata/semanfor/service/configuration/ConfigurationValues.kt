package cz.petrjanata.semanfor.service.configuration

/**
 * This class provides custom configuration, that should be provided by an admin.
 */
object ConfigurationValues {
    val SUPPORTED_EMAIL_POSTFIXES: Array<String> = arrayOf("@eman.cz")
}