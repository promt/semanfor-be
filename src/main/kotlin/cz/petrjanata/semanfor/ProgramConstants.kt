package cz.petrjanata.semanfor

import java.io.InputStreamReader
import java.io.Reader
import java.net.URL

/**
 * This class contains constants that affects only this program.
 */
object ProgramConstants {

    ///Google
    private const val GOOGLE_CLIENT_SECRET_FILE_NAME = "client_secret.apps.googleusercontent.com.json"
    const val GOOGLE_CLIENT_SECRET_FILE_PATH = "files/$GOOGLE_CLIENT_SECRET_FILE_NAME"
    val GOOGLE_CLIENT_SECRET_FILE_URL: URL by lazy {
        val classLoader = Application::class.java.classLoader
        return@lazy classLoader.getResource(GOOGLE_CLIENT_SECRET_FILE_PATH)
    }


}