package cz.petrjanata.semanfor.usecase.google


import com.github.salomonbrys.kodein.instance
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier
import com.google.api.client.http.apache.ApacheHttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import cz.petrjanata.semanfor.Application
import cz.petrjanata.semanfor.helper.log.SLog
import cz.petrjanata.semanfor.model.AuthorizationException
import cz.petrjanata.semanfor.service.configuration.ConfigurationValues
import java.util.*


class VerifyTokenUsecase {

    private val configurationValues: ConfigurationValues = Application.kodein.instance()


    /**
     * @param clientId client id of mobile app. You can find this in credentials.json
     * @param idTokenString user token
     */
    @Throws(AuthorizationException::class)
    fun verify(clientId: String, idTokenString: String): Boolean {
        val transport = ApacheHttpTransport()
        val jsonFactory = JacksonFactory.getDefaultInstance()

        val verifier: GoogleIdTokenVerifier = GoogleIdTokenVerifier.Builder(transport, jsonFactory)
                // Specify the CLIENT_ID of the app that accesses the backend:
                .setAudience(Collections.singletonList(clientId))
                // Or, if multiple clients access the backend:
                //.setAudience(Arrays.asList(CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3))
                .build()


        // (Receive idTokenString by HTTPS POST)
        val idToken = verifier.verify(idTokenString)
        if (idToken != null) {
            val payload = idToken.getPayload()
            return isPayloadValid(payload)
        } else {
            return false
        }
    }

    @Throws(AuthorizationException::class)
    fun isPayloadValid(payload: GoogleIdToken.Payload): Boolean {

        // Print user identifier
        val userId = payload.getSubject()
        SLog.d("User ID: $userId")

        // Get profile information from payload
        val email = payload.getEmail()
        val emailVerified = java.lang.Boolean.valueOf(payload.emailVerified)
        if (!emailVerified) {
            val unauthorizedEx = AuthorizationException("User email was not verified.", AuthorizationException.Reason.FORBIDDEN_EMAIL)
            SLog.e("User email $email was not verified.", unauthorizedEx)
            throw unauthorizedEx
        }

        return isEmailValid(email)
    }

    private fun isEmailValid(email: String): Boolean {
        val postfixes = configurationValues.SUPPORTED_EMAIL_POSTFIXES

        var contained: Boolean = false
        postfixes.forEach { postfix ->
            contained = contained || email.contains(postfix, true)
        }

        return contained
    }
}
