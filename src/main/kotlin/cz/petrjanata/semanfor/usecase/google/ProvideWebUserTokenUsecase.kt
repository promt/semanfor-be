package cz.petrjanata.semanfor.usecase.google

import com.github.salomonbrys.kodein.instance
import com.google.api.client.auth.oauth2.TokenResponseException
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import cz.petrjanata.semanfor.Application
import cz.petrjanata.semanfor.ProgramConstants
import cz.petrjanata.semanfor.model.User
import cz.petrjanata.semanfor.model.AuthorizationException
import cz.petrjanata.semanfor.service.web.google.GoogleWebConstants
import java.io.IOException
import java.io.InputStreamReader


class ProvideWebUserTokenUsecase {

    private val programConstants: ProgramConstants = Application.kodein.instance()
    private val webConstants: GoogleWebConstants = Application.kodein.instance()

    private val validationUsecase: VerifyTokenUsecase = Application.kodein.instance()

    /**
     * @param  serverAuthCode e. g. "4/AAC9MDZaHcQ1nB3pb7g_RWn1OAXZ2MVq5Oedqj8gOSKlQILI-fT2C7-prjCSPx2Em7XmIl5ovwPzBgY7tkV6Sjc"
     * @return accessToken if user verified and null if verification failed
     */
    @Throws(IOException::class, AuthorizationException::class)
    fun provideUserInfo(user: User) : User {
        // Set path to the Web application client_secret_*.json file you downloaded from the
        // Google API Console: https://console.developers.google.com/apis/credentials
        // You can also find your Web application client ID and client secret from the
        // console and specify them directly when you create the GoogleAuthorizationCodeTokenRequest
        // object.
        val fileUrl = programConstants.GOOGLE_CLIENT_SECRET_FILE_URL
        val stream = fileUrl.openStream()
        val fileReader = InputStreamReader(stream)

        // Exchange auth code for access token
        val clientSecrets = GoogleClientSecrets.load(
                JacksonFactory.getDefaultInstance(), fileReader)
        val serverAuthCode = user.serverAuthCode
        val tokenResponse: GoogleTokenResponse
        try {

            tokenResponse = GoogleAuthorizationCodeTokenRequest(
                    NetHttpTransport(),
                    JacksonFactory.getDefaultInstance(),
                    webConstants.GOOGLE_URL_TOKEN_SERVER_ENCODED,
                    clientSecrets.details.clientId,
                    clientSecrets.details.clientSecret,
                    serverAuthCode,
                    "")  // Specify the same redirect URI that you use with your web
                    // app. If you don't have a web version of your app, you can
                    // specify an empty string.
                    .execute()
        } catch (ex: TokenResponseException) {
            val authorizationException = AuthorizationException("User server Auth Code \"$serverAuthCode\" is invalid.", ex)
            authorizationException.reason = AuthorizationException.Reason.INVALID_TOKEN
            throw authorizationException
        } catch (ex: IOException) {
            throw ex
        }

        val accessToken: String = tokenResponse.accessToken


        // Get profile info from ID token
        val idToken = tokenResponse.parseIdToken()
        val payload: GoogleIdToken.Payload = idToken.payload

        //verify if user has right to access
        if (!validationUsecase.isPayloadValid(payload)) {
            throw AuthorizationException("User email was not verified.", AuthorizationException.Reason.FORBIDDEN_DOMAIN)
        }

        //todo remove these payload info if no longer needed
        val name = payload[webConstants.GOOGLE_KEY_USER_NAME] as String
        val pictureUrl = payload[webConstants.GOOGLE_KEY_USER_PICTURE] as String
        val locale = payload[webConstants.GOOGLE_KEY_USER_LOCALE] as String
        val familyName = payload[webConstants.GOOGLE_KEY_USER_FAMILY_NAME] as String
        val givenName = payload[webConstants.GOOGLE_KEY_USER_GIVEN_NAME] as String
//        val gender = payload["gender"] as String

        user.email = payload.email
        user.token = accessToken

        // Use or store profile information
        // ...


        return user
    }

}