package cz.petrjanata.semanfor.usecase.google

import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.Application
import cz.petrjanata.semanfor.api.model.PlaceADO
import cz.petrjanata.semanfor.helper.log.SLog
import cz.petrjanata.semanfor.model.*
import cz.petrjanata.semanfor.service.web.WebResponse
import cz.petrjanata.semanfor.service.web.google.GoogleWebService
import cz.petrjanata.semanfor.service.web.google.model.EventDTO
import cz.petrjanata.semanfor.service.web.google.model.PersonDTO
import cz.petrjanata.semanfor.service.web.google.model.ResponseStatus
import java.util.*

class EventUsecase {

    private val googleWebService: GoogleWebService = Application.kodein.instance()

    @Throws(ConnectionException::class)
    fun createEvent(place: PlaceADO, user: User, since: Date, until: Date): EventDTO {

        val eventName: String = "Parking: "+user.email
        val attendees = ArrayList<PersonDTO>()

        val room: PersonDTO = PersonDTO(email = place.calendarID, responseStatus = ResponseStatus.needsAction)
        attendees.add(room)

        val event = EventDTO(eventName, since.time, until.time, attendees)


        val response = googleWebService.createEvent(user.token , user.calendarID, event)

        return parseEventDTOWrapper(response)
    }

    @Throws(WrongIdException::class, ConnectionException::class)
    fun editEvent(event: EventDTO, user: User): EventDTO {
        val eventID: String = event.id ?: throw WrongIdException("Event id is null. $event")
        val response = googleWebService.updateEvent(user.token, user.calendarID, eventID, event)
        return parseEventDTOWrapper(response)
    }

    @Throws(WrongIdException::class, ConnectionException::class)
    fun deleteEvent(event: EventDTO, user: User) {
        val eventID: String = event.id ?: throw WrongIdException("Event id is null. $event")
        val response: IValueWrapper<WebResponse<EventDTO>, ConnectionException> = googleWebService.deleteEvent(user.token, user.calendarID, eventID)

        val exception = response.exception
        exception?.let {
            SLog.e(javaClass.simpleName, it)
            throw exception
        }

        val webResponse = response.value ?: throw IllegalStateException("Response must contain either exception or value: $response")

        val code = webResponse.code
        if (code < 200 || code >= 300) {
            throw ResponseValidationException("Unsupported response code $code when deleting event $eventID.")
        } else if (code != 204) {
            SLog.w("Unexpected status code $code when deleting event $eventID.")
        }

        return
    }

    @Throws(WrongIdException::class, ConnectionException::class)
    fun getEventById(eventId: String, user: User): EventDTO {
        val response = googleWebService.getEvent(user.token, user.calendarID, eventId)
        return parseEventDTOWrapper(response)
    }

    @Throws(ConnectionException::class)
    fun getEventsForPlace(user: User, place: PlaceADO, intervalSince: Date, intervalUntil: Date): Collection<EventDTO> {
        val response = googleWebService.getEvents(user.token, place.calendarID, intervalSince, intervalUntil, null)
        val exception = response.exception
        exception?.let {
            SLog.e(javaClass.simpleName, it)
            throw exception
        }

        val eventsList = response.value
        if (eventsList == null) {
            throw IllegalStateException("Response must contain either exception or value: $response")
        }

        return eventsList.events
    }

    ///private methods
    @Throws(ConnectionException::class)
    private fun <E: ConnectionException> parseEventDTOWrapper(response: IValueWrapper<EventDTO, E>): EventDTO {
        val exception = response.exception
        exception?.let {
            SLog.e(javaClass.simpleName, it)
            throw exception
        }

        val eventDTO: EventDTO? = response.value
        if (eventDTO == null) {
            throw IllegalStateException("Response must contain either exception or value: $response")
        }

        return eventDTO
    }
}