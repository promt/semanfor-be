package cz.petrjanata.semanfor.usecase

import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.Application
import cz.petrjanata.semanfor.api.model.DriverADO
import cz.petrjanata.semanfor.service.configuration.IDriversProvider
import cz.petrjanata.semanfor.service.web.google.model.EventDTO

class DriverUsecase {

    private val driversProvider: IDriversProvider = Application.kodein.instance()

    fun getDriverForEvent(eventDTO: EventDTO): DriverADO? {
        val creatorEmail: String = eventDTO.creator.email
        val searchedDriver = driversProvider.allDrivers.firstOrNull { driver: DriverADO ->
            driver.email == creatorEmail
        }
        return searchedDriver
    }

}