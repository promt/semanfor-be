package cz.petrjanata.semanfor.usecase.place

import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.Application
import cz.petrjanata.semanfor.api.model.PlaceADO
import cz.petrjanata.semanfor.model.WrongIdException
import cz.petrjanata.semanfor.service.configuration.IParkPlacesProvider

class GetPlaceUsecase {

    private val parkPlacesProvider: IParkPlacesProvider = Application.kodein.instance()

    @Throws(WrongIdException::class)
    fun getPlaceById(placeId: String): PlaceADO {
        val filteredPlaces: List<PlaceADO> = parkPlacesProvider.allPlaces.filter { place: PlaceADO ->
            place.calendarID == placeId
        }

        if (filteredPlaces.isEmpty()) {
            throw WrongIdException("No place found with semanforID: $placeId.")
        }

        return filteredPlaces.first()
    }

    fun getAllPlacesForReservation(): Collection<PlaceADO> {
        return parkPlacesProvider.placesForReservation
    }
}