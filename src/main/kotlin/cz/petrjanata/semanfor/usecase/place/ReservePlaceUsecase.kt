package cz.petrjanata.semanfor.usecase.place

import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.Application
import cz.petrjanata.semanfor.api.model.AllocationADO
import cz.petrjanata.semanfor.api.model.PlaceADO
import cz.petrjanata.semanfor.model.User
import cz.petrjanata.semanfor.model.WrongIdException
import cz.petrjanata.semanfor.service.web.google.model.EventDTO
import cz.petrjanata.semanfor.service.web.google.model.EventTime
import cz.petrjanata.semanfor.usecase.google.EventUsecase
import java.util.*

class ReservePlaceUsecase {

    private val getPlaceUsecase: GetPlaceUsecase = Application.kodein.instance()
    private val eventUsecase: EventUsecase = Application.kodein.instance()

    @Throws(WrongIdException::class)
    fun makeAllocation(placeId: String, user: User, since: Date, until: Date): AllocationADO {
        val place: PlaceADO = getPlaceUsecase.getPlaceById(placeId)
        val eventDTO = eventUsecase.createEvent(place, user, since, until)


        val sinceCreated: Date = eventDTO.start.getDate()
        val untilCreated: Date = eventDTO.end.getDate()
        val eventID = eventDTO.id

        return AllocationADO(eventID, sinceCreated, untilCreated, place)
    }

    @Throws(WrongIdException::class)
    fun editAllocation(allocation: AllocationADO, user: User): AllocationADO {
        val oldEvent: EventDTO = eventUsecase.getEventById(allocation.eventID, user)
        oldEvent.start = EventTime(allocation.since)
        oldEvent.end = EventTime(allocation.until)

        val newEvent = eventUsecase.editEvent(oldEvent, user)

        var place: PlaceADO? = null
        val placeEmail = newEvent.attendees.firstOrNull()?.email
        placeEmail?.let { place = getPlaceUsecase.getPlaceById(it)}
        val placeVal = place ?: throw WrongIdException("Place with Google id $placeEmail not found.")


        val sinceCreated: Date = newEvent.start.getDate()
        val untilCreated: Date = newEvent.end.getDate()
        val eventID = newEvent.id

        return AllocationADO(eventID, sinceCreated, untilCreated, placeVal)
    }


    @Throws(WrongIdException::class)
    fun deleteAllocation(allocation: AllocationADO, user: User) {
        val oldEvent: EventDTO = eventUsecase.getEventById(allocation.eventID, user)
        eventUsecase.deleteEvent(oldEvent, user)
    }
}