package cz.petrjanata.semanfor.usecase.place

import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.Application
import cz.petrjanata.semanfor.api.model.*
import cz.petrjanata.semanfor.helper.ExecutionHelper
import cz.petrjanata.semanfor.model.User
import cz.petrjanata.semanfor.service.web.google.model.EventDTO
import cz.petrjanata.semanfor.service.web.google.model.PersonDTO
import cz.petrjanata.semanfor.usecase.DriverUsecase
import cz.petrjanata.semanfor.usecase.firebase.FirebaseUsecase
import cz.petrjanata.semanfor.usecase.google.EventUsecase
import java.util.*
import kotlin.collections.HashSet

class RequestPlacesUsecase {

    private val getPlaceUsecase: GetPlaceUsecase = Application.kodein.instance()
    private val eventUsecase: EventUsecase = Application.kodein.instance()
    private val firebaseUsecase: FirebaseUsecase = Application.kodein.instance()
    private val driverUsecase: DriverUsecase = Application.kodein.instance()
    private val executionHelper: ExecutionHelper = Application.kodein.instance()

    fun requestPlacesAsync(user: User, intervalSince: Date, intervalUntil: Date): Unit {
        executionHelper.executeAsync {
            requestPlaces(user, intervalSince, intervalUntil)
        }
    }

    fun requestPlaces(user: User, intervalSince: Date, intervalUntil: Date) {
        val emptyPlaces: MutableCollection<PlaceADO> = mutableSetOf()
        val fullPlaces: HashSet<PlaceADO> = HashSet()
        var userAllocation: AllocationADO? = null

        val allPlaces: Collection<PlaceADO> = getPlaceUsecase.getAllPlacesForReservation()

        allPlaces.forEach { place ->
            val events: Collection<EventDTO> = eventUsecase.getEventsForPlace(user, place, intervalSince, intervalUntil)
            val event: EventDTO? = findUserAllocation(user, events)
            if (event != null) {
                userAllocation = assembleUserAllocation(event, place)
            }

            if (events.isEmpty()) {
                emptyPlaces.add(place)
            } else {
                place.driver = driverUsecase.getDriverForEvent(event ?: events.first())
                fullPlaces.add(place)
            }
        }

        val lotState: LotState = determinateLotState(userAllocation, emptyPlaces)
        val driverInfo = DriverInfoADO(lotState, emptyPlaces, fullPlaces, userAllocation)

        sendPushToUser(user, driverInfo)
    }

    private fun findUserAllocation(user: User, events: Collection<EventDTO>): EventDTO? {
        events.forEach { event ->
            val creator: PersonDTO = event.creator
            if (user.email == creator.email) {
                return event
            }
        }

        return null
    }

    private fun assembleUserAllocation(event: EventDTO?, placeADO: PlaceADO): AllocationADO? {
        if (event == null) {
            return null
        }
        return AllocationADO(event.id, event.start.getDate(), event.end.getDate(), placeADO)
    }

    private fun determinateLotState(hasUserAllocation: AllocationADO?, emptyPlaces: Collection<PlaceADO>): LotState {
        when {
            hasUserAllocation != null -> return LotState.RESERVATION
            emptyPlaces.isEmpty() -> return LotState.FULL
            else -> return LotState.FREE
        }
    }

    private fun sendPushToUser(user: User, driverInfoADO: DriverInfoADO) {
        firebaseUsecase.sendDriverInfo(user, driverInfoADO)
    }
}