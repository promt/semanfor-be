package cz.petrjanata.semanfor.usecase.firebase

import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.Application
import cz.petrjanata.semanfor.api.model.DriverInfoADO
import cz.petrjanata.semanfor.api.model.PushDataWrapperADO
import cz.petrjanata.semanfor.model.User
import cz.petrjanata.semanfor.helper.log.SLog
import cz.petrjanata.semanfor.model.ConnectionException
import cz.petrjanata.semanfor.model.IValueWrapper
import cz.petrjanata.semanfor.service.web.firebase.FirebaseWebService
import cz.petrjanata.semanfor.service.web.firebase.model.FirebaseResponseDTO

class FirebaseUsecase {

    private val webService: FirebaseWebService = Application.kodein.instance()

    fun sendDriverInfo(user: User, driverInfo: DriverInfoADO): Boolean {
        val dataWrapper = PushDataWrapperADO(driverInfo)

        val response = webService.sendPush(user.pushToken, dataWrapper)

        val firebaseResponseDTO = parseFirebaseResponseDTOWrapper(response)

        if (firebaseResponseDTO.failure > 0 || firebaseResponseDTO.success <= 0) {
            val results = firebaseResponseDTO.results.toString()
            SLog.e("Firebase unable to send push. Results: $results")
            return false
        }

        return true
    }


    @Throws(ConnectionException::class)
    private fun <E: ConnectionException> parseFirebaseResponseDTOWrapper(response: IValueWrapper<FirebaseResponseDTO, E>): FirebaseResponseDTO {
        val exception = response.exception
        exception?.let {
            SLog.e(javaClass.simpleName, it)
            throw it
        }

        val firebaseResponseDTO: FirebaseResponseDTO? = response.value
        if (firebaseResponseDTO == null) {
            throw IllegalStateException("Response must contain either exception or value: $response")
        }

        return firebaseResponseDTO
    }
}