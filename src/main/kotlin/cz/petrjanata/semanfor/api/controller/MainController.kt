package cz.petrjanata.semanfor.api.controller

import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.Application
import cz.petrjanata.semanfor.api.ApiConstants
import cz.petrjanata.semanfor.api.model.AllocationADO
import cz.petrjanata.semanfor.api.model.CreateAllocationBodyADO
import cz.petrjanata.semanfor.api.model.EditAllocationBodyADO
import cz.petrjanata.semanfor.model.User
import cz.petrjanata.semanfor.helper.log.SLog
import cz.petrjanata.semanfor.usecase.google.ProvideWebUserTokenUsecase
import cz.petrjanata.semanfor.usecase.place.RequestPlacesUsecase
import cz.petrjanata.semanfor.usecase.place.ReservePlaceUsecase
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.text.ParseException
import java.util.*

@RestController
class MainController : ExceptionController() {

    private val provideWebUserTokenUsecase: ProvideWebUserTokenUsecase = Application.kodein.instance()
    private val reservePlaceUsecase: ReservePlaceUsecase = Application.kodein.instance()
    private val apiConstants: ApiConstants = Application.kodein.instance()
    private val requestPlacesUsecase: RequestPlacesUsecase = Application.kodein.instance()


    @RequestMapping(path = ["/"+apiConstants.END_POINT_PLACES], method = [RequestMethod.GET])
    internal fun getAvailablePlaces(@RequestParam(name = apiConstants.PARAM_SERVER_AUTH_CODE) serverAuthCode: String,
                                    @RequestParam(name = apiConstants.PARAM_PUSH_TOKEN) pushToken: String,
                                    @RequestParam(name = apiConstants.PARAM_SINCE) sinceString: String,
                                    @RequestParam(name = apiConstants.PARAM_UNTIL) untilString: String): ResponseEntity<Any> {
        val sinceDate: Date
        val untilDate: Date

        try {
            sinceDate = apiConstants.dateFormat.parse(sinceString)
            untilDate = apiConstants.dateFormat.parse(untilString)
        } catch (ex: ParseException) {
            val responseMessage = "Date parameter is not parsable.\n" + ex.message
            SLog.i(responseMessage, ex)
            return ResponseEntity(responseMessage, HttpStatus.BAD_REQUEST)
        }


        val user: User = provideWebUserTokenUsecase.provideUserInfo(User(serverAuthCode, pushToken))

        requestPlacesUsecase.requestPlacesAsync(user, sinceDate, untilDate)
        return ResponseEntity(HttpStatus.ACCEPTED)
    }


    @RequestMapping(path = ["/"+apiConstants.END_POINT_ALLOCATION], method = [RequestMethod.POST])
    internal fun createAllocation(@RequestBody body: CreateAllocationBodyADO) : ResponseEntity<AllocationADO> {
        val user: User = provideWebUserTokenUsecase.provideUserInfo(User(body.serverAuthCode))
        val allocation: AllocationADO = reservePlaceUsecase.makeAllocation(body.placeID, user, body.since, body.until)
        return ResponseEntity(allocation, HttpStatus.CREATED)
    }

    @RequestMapping(path = ["/"+apiConstants.END_POINT_ALLOCATION], method = [RequestMethod.PATCH])
    internal fun editAllocation(@RequestBody body: EditAllocationBodyADO) : ResponseEntity<Any> {
        val user: User = provideWebUserTokenUsecase.provideUserInfo(User(body.serverAuthCode))

        val since: Date = body.allocation.since
        val until: Date = body.allocation.until
        if (since.time > until.time) {
            return ResponseEntity("Since {$since} can not be later than until {$until}.", HttpStatus.BAD_REQUEST)
        }

        val allocation: AllocationADO = reservePlaceUsecase.editAllocation(body.allocation, user)
        return ResponseEntity(allocation, HttpStatus.OK)
    }

    @RequestMapping(path = ["/"+apiConstants.END_POINT_ALLOCATION], method = [RequestMethod.DELETE])
    internal fun deleteAllocation(@RequestBody body: EditAllocationBodyADO) : ResponseEntity<Any> {
        val user: User = provideWebUserTokenUsecase.provideUserInfo(User(body.serverAuthCode))

        val since: Date = body.allocation.since
        val until: Date = body.allocation.until
        if (since.time > until.time) {
            return ResponseEntity("Since {$since} can not be later than until {$until}.", HttpStatus.BAD_REQUEST)
        }

        reservePlaceUsecase.deleteAllocation(body.allocation, user)
        return ResponseEntity(HttpStatus.NO_CONTENT)
    }
}


