package cz.petrjanata.semanfor.api.controller

import cz.petrjanata.semanfor.helper.log.SLog
import cz.petrjanata.semanfor.model.AuthorizationException
import cz.petrjanata.semanfor.model.WrongIdException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ExceptionHandler
import java.io.IOException

abstract class ExceptionController() {

    //https://www.baeldung.com/exception-handling-for-rest-with-spring
    @ExceptionHandler(Exception::class)
    fun handleException(ex: Exception) : ResponseEntity<Any> {

        var responseEntity: ResponseEntity<Any>? = null
        var resultException: Exception? = null

        when (ex) {

            is AuthorizationException -> {
                ex.reason.hashCode()
                when (ex.reason) {
                    AuthorizationException.Reason.FORBIDDEN_DOMAIN, AuthorizationException.Reason.FORBIDDEN_EMAIL -> {
                        responseEntity = ResponseEntity(HttpStatus.FORBIDDEN)
                    }
                    AuthorizationException.Reason.INVALID_TOKEN -> responseEntity = ResponseEntity(HttpStatus.UNAUTHORIZED)
                    else -> resultException = Exception("Unknown reason ${ex.reason} of AuthorizationException.", ex)
                }
            }

            is WrongIdException -> {
                val message: String = ex.javaClass.simpleName + ": " + ex.message
                responseEntity = ResponseEntity(message, HttpStatus.BAD_REQUEST)
            }

            is IOException -> {
                val message = "${ex.javaClass.simpleName}: ${ex.message}\n $ex"
                responseEntity = ResponseEntity(message, HttpStatus.INTERNAL_SERVER_ERROR)
            }

            else -> resultException = ex
        }


        if (resultException != null) {
            SLog.e("Unhandled exception ${resultException.javaClass.simpleName}", resultException)
            throw resultException
        }

        SLog.i("Exception ${ex.javaClass.simpleName} handled as $responseEntity", ex)
        return responseEntity ?: throw IllegalStateException("Either resultException or responseEntity must be filled.")
    }

}