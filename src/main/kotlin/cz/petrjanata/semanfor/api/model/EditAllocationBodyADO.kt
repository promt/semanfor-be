package cz.petrjanata.semanfor.api.model


class EditAllocationBodyADO() {
    lateinit var serverAuthCode: String
    lateinit var allocation: AllocationADO

    constructor(serverAuthCode: String, allocation: AllocationADO) : this() {
        this.serverAuthCode = serverAuthCode
        this.allocation = allocation
    }
}