package cz.petrjanata.semanfor.api.model

class DriverInfoADO {
    lateinit var lotState: LotState
             var freePlaces: Collection<PlaceADO>? = null
             var fullPlaces: Collection<PlaceADO>? = null
             var allocation: AllocationADO? = null

    constructor()

    constructor(lotState: LotState, freePlaces: Collection<PlaceADO>?, fullPlaces: Collection<PlaceADO>?, allocation: AllocationADO?) {
        this.lotState = lotState
        this.freePlaces = freePlaces
        this.fullPlaces = fullPlaces
        this.allocation = allocation
    }
}