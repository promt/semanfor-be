package cz.petrjanata.semanfor.api.model

class PlaceADO : Comparable<PlaceADO> {

    lateinit var caption: String
    lateinit var name: String
    lateinit var calendarID: String
    var driver: DriverADO? = null

    //for json serialization
    constructor()

    constructor(caption: String, name: String, calendarID: String, driver: DriverADO? = null) {
        this.caption = caption
        this.name = name
        this.calendarID = calendarID
        this.driver = driver
    }

    override fun compareTo(other: PlaceADO): Int {
        return caption.compareTo(other.caption)
    }

}