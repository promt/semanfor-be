package cz.petrjanata.semanfor.api.model

class PushDataWrapperADO() {

    lateinit var driverInfo: DriverInfoADO

    constructor(driverInfo: DriverInfoADO): this() {
        this.driverInfo = driverInfo
    }
}