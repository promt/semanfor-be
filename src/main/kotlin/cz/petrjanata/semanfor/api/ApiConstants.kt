package cz.petrjanata.semanfor.api

import java.text.SimpleDateFormat
import java.util.*

/**
 * This class is holder for constants used in public API interface of this application.
 * By changing any of these values you change communication interface for all clients - e. g. mobile apps.
 */
object ApiConstants {

    const val CONTENT_TYPE = "Content-Type"
    const val APPLICATION_JSON = "application/json"


    const val JSON_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    internal val dateFormat = SimpleDateFormat(JSON_DATE_FORMAT, Locale.US)

    const val END_POINT_PLACES = "places"
    const val END_POINT_ALLOCATION = "allocation"

    const val PARAM_SERVER_AUTH_CODE = "serverAuthCode"
    const val PARAM_PUSH_TOKEN = "pushToken"
    const val PARAM_SINCE = "since"
    const val PARAM_UNTIL = "until"

    ///Google
    ///Google scope
    const val GOOGLE_SCOPE_CALENDAR = "https://www.googleapis.com/auth/calendar"

    ///Google config
    const val GOOGLE_WEB_CLIENT_ID = "516434616522-llcolbe8h6lf637p5d6f7qnrl8t93ufm.apps.googleusercontent.com"

}