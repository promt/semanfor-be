package cz.petrjanata.semanfor

import com.github.salomonbrys.kodein.Kodein
import cz.petrjanata.semanfor.helper.inject.SemanforKodeinHelper
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
open class Application {

    companion object {
        val kodein: Kodein by lazy { SemanforKodeinHelper().provideKodein() }

        @JvmStatic fun main(args: Array<String>) {
            SpringApplication.run(Application::class.java, *args)
//            com.example.Application.main(args)
        }
    }
}