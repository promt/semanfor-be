package cz.petrjanata.semanfor.test

import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.Application
import cz.petrjanata.semanfor.api.ApiConstants
import cz.petrjanata.semanfor.helper.log.SLog
import org.junit.Assert
import org.junit.Test
import java.text.SimpleDateFormat
import java.util.*

class DateParsingTest {

    private val stringDate: String = "2018-10-26T14:08:52.137+0200"
    private val longDate: Long = 1540555732137L
    private val apiConstants: ApiConstants = Application.kodein.instance()

    @Test
    fun testDateParsing() {
        val dateFormat: SimpleDateFormat = apiConstants.dateFormat
        val fromLong: Date = Date(longDate)
        val formattedFromLong: String = dateFormat.format(fromLong)
        val fromString: Date = dateFormat.parse(stringDate)
        SLog.d("Long: $longDate")
        SLog.d("Date from long: $fromLong")
        SLog.d("Formatted from date fromLong: $formattedFromLong")

        Assert.assertEquals(fromLong, fromString)
        Assert.assertEquals(formattedFromLong, stringDate)

    }


}