package cz.petrjanata.semanfor.test

import org.junit.Assert

class SampleTest {

    private val first = 1
    private val second = 2
    private val expectedResult = 3

    @org.junit.Test
    fun testSample(): Unit {
        val sum = first + second
        Assert.assertEquals(expectedResult, sum)
    }
}