package cz.petrjanata.semanfor.test

import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.Application
import cz.petrjanata.semanfor.ProgramConstants
import org.junit.Assert
import org.junit.Test

class KodeinTest {

    private val programConstants: ProgramConstants = Application.kodein.instance()

    @Test
    fun testInjection() {
        testSimpleInjection()
    }

    private fun testSimpleInjection() {
        Assert.assertNotNull(programConstants)
    }
}