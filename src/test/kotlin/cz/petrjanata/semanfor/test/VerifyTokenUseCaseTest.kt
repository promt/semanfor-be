package cz.petrjanata.semanfor.test

import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.Application
import cz.petrjanata.semanfor.usecase.google.VerifyTokenUsecase
import org.junit.Assert

class VerifyTokenUseCaseTest {

    private val clientId: String = TODO("Provide client id for your project. (non-publicable information)")
    private val idTokenString = getToken()
    private val usecase: VerifyTokenUsecase = Application.kodein.instance()

//    @Test
    fun testValidation() {
        testInstance(usecase)
        val verified = usecase.verify(clientId, idTokenString)
        Assert.assertTrue(verified)
    }

    private fun testInstance(usecase: VerifyTokenUsecase) {
        Assert.assertNotNull(usecase)
    }

    private fun getToken(): String {
        //for example "eyJhbGciOiJSUzI1NiIsImtpZCI6IjQwYzZiMDliNDQ5NjczNDUzYzNkYTY5OWUyZGY1NTI3ZjkxZTY4MDMifQ.eyJhenAiOiI1MTY0MzQ2MTY1MjItYW1rOGdibGc2Z25pZms3b2I2cWw1OGJpZGMxM2ZwNHEuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiI1MTY0MzQ2MTY1MjItbGxjb2xiZThoNmxmNjM3cDVkNmY3cW5ybDh0OTN1Zm0uYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMDE0MTIxNjg5Mzg2MTAzNzIzODAiLCJoZCI6ImVtYW4uY3oiLCJlbWFpbCI6InBldHIuamFuYXRhQGVtYW4uY3oiLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiZXhwIjoxNTMyNzk0MjgxLCJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJpYXQiOjE1MzI3OTA2ODEsIm5hbWUiOiJQZXRyIEphbmF0YSIsInBpY3R1cmUiOiJodHRwczovL2xoMy5nb29nbGV1c2VyY29udGVudC5jb20vLTE4UjBLQm1LdG9VL0FBQUFBQUFBQUFJL0FBQUFBQUFBQUFBL29IQm1JYVZMUlFJL3M5Ni1jL3Bob3RvLmpwZyIsImdpdmVuX25hbWUiOiJQZXRyIiwiZmFtaWx5X25hbWUiOiJKYW5hdGEiLCJsb2NhbGUiOiJjcyJ9.OLUorsTEX8sa2JlPc8v39ImNY2MLp-SUjyTFYEql-GyvZDEq6v_iJEUeebTKfKeT6JfFhpu5MU0uIXbIZSixyqfv4uTZvpUJS17qkgKTANlxhCtfqxA_uvKZG0UuALI855Ge7ul4Ew6QXuKXVRY8xL5-VHWZq1mZLT62_49zf212kPOibzs6OFvufSVJ0pKvIREaakNK4b11WKFvD_9slV_IL-fD4QXQWeW7nl1aw607SNNnFLNkqN9b72stZwkZYRE32mt25it2RZ0tT5ybPqJk-UBOi4W3LijFdDmxHfIFk-a_UNyZ3ydelN_EaeGQ4vfprWrhT2sEq2A_9cTnzQ"
        return TODO("Provide token for your client. (variable value)")
    }
}