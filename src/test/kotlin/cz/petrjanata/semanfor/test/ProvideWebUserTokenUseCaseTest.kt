package cz.petrjanata.semanfor.test

import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.Application
import cz.petrjanata.semanfor.model.User
import cz.petrjanata.semanfor.helper.log.SLog
import cz.petrjanata.semanfor.usecase.google.ProvideWebUserTokenUsecase
import org.junit.Assert

class ProvideWebUserTokenUseCaseTest {

    private val usecase: ProvideWebUserTokenUsecase = Application.kodein.instance()
    private val serverAuthCode: String = TODO("Provide serverAuthCode for your client. (variable value)")
                                //for example "4/dwBfXCkgS8tDiWIbh_aSrz4WfuyUJH0vq_SeMaDe9XeYifDxuT_1vw_qRv6YpvB9fXr5HKMmavU7n2agFbxQE7U"

//    @Test  //note: remember that you need to provide new serverAuthCode for each run of the test
    fun testUseCase() {
        val filledUser = usecase.provideUserInfo(User(serverAuthCode))
        SLog.i("accessToken", filledUser.token)
        Assert.assertNotNull(filledUser.token)
    }
}